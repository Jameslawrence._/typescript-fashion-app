import React, {Fragment} from "react";
import HeaderCartButton from "./HeaderCartButton";

import "./Header.css";

const Header: React.FC = () => {
	const ImageBanner = require("../../assets/banner.jpg");
	return <Fragment>
		<header className="header">
			<h1>fakeStoreAPI.com</h1>
			<HeaderCartButton/>
		</header>
		<div className="main-image">
			<img src={ImageBanner} alt='a table of full of delicious food'/>
		</div>
	</Fragment>
}

export default Header; 