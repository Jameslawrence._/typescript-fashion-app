import React from "react";
import {useEffect, useState} from 'react';
import Item from "./Item";
import Card from "../UI/Card/Card";

const ItemAvailable: React.FC = () => {

	const [products, setProducts] = useState([]);

	useEffect(() => {
		fetch('https://fakestoreapi.com/products/')
	    .then(res=>res.json())
	    .then(data=> {
			const itemList = data.map((item:any) => {
				return <Item
					key={item.id}
					id={item.id}
					name={item.title}
					description={item.description}
					image= {item.image}
					price={item.price}
				/>

			});
			setProducts(itemList);
	    })
	},[]);		

	return <section className="items">
		<Card>	
			<ul>{products}</ul>
		</Card>
	</section>	
}

export default ItemAvailable;