import React from "react";
import "./Item.css";

interface ItemProps {
	id: Number,
	image: String, 
	name: String,
	description: String, 
	price: Number
}


const Item: React.FC<ItemProps> = ({id,image, name,description,price}:ItemProps) => {
	
	const priceFixed = `$${price.toFixed(2)}`;	

	return <li className="item">	
		<div>	
			{/*<img src={image} alt={name}/>*/}
			<h3>{name}</h3>
			<div className="description">{description}</div>
			<div className="price">{priceFixed}</div>
		</div>
	</li>	
}

export default Item;