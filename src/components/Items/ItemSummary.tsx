import React from "react";
import "./ItemSummary.css";

const ItemSummary: React.FC = () => {
	return <section className="summary">
		<h2>Fashion, Delivered To You</h2>
		<p>
			Choose your favorite fashion item from our broad selection of available items 
			and feel like a star in the spotlight.
		</p>
		<p>
			All our items are high-quality, just in time for the fashion get away!
		</p>
	</section>		

}

export default ItemSummary;