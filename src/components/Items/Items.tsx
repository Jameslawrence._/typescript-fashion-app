import React, {Fragment} from "react";
import ItemSummary from "./ItemSummary";
import ItemAvailable from "./ItemAvailable";

const Items: React.FC = () => {
	return <Fragment>	
		<ItemSummary/>
		<ItemAvailable/>
	</Fragment>	
}

export default Items;