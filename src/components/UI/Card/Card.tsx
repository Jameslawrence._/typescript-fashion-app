import React from "react";
import "./Card.css";

interface CardProps{
	//className: String,
	children: React.ReactNode
}

const Card: React.FC<CardProps> = (props: CardProps) => {
	//const cardClasses = `card ${props.className}`;

	return <div className="card">
		{props.children}
	</div>
}

export default Card;