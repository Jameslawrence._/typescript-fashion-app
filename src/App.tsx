import React from "react";
import "./App.css";
import Header from "./components/Layout/Header";
import Items from "./components/Items/Items";

const App: React.FunctionComponent = () => {
  return <div className="App">
    <Header/>
    <Items/>
  </div>
}

export default App;
